from django.db import models


class Rate(models.Model):
    isp = models.CharField(max_length=80, db_index=True)
    quote = models.FloatField(null=True)
    cost = models.FloatField(null=True)

    def __str__(self):
        return self.isp
