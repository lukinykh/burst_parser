from .models import Rate
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth.views import LoginView
from django.http.response import HttpResponse
from django.http import Http404
from django.conf import settings
from django.core.cache import cache

import json
import calendar
import mimetypes
from openpyxl.utils.exceptions import InvalidFileException

from .forms import LoginForm, MonthPickerForm, RateForm

from .utils.parse_burst import ParseBurst, create_xlsx, calculate_amount


def main(request):
    return render(request, 'burst_app/main.html')


@login_required
def home(request):
    return render(request, 'burst_app/home.html')


@login_required
def error(request):
    if cache.get('empty_report'):
        return render(request, 'burst_app/error.html', {'error': 'В отчете отсутствуют данные за указанный период. Выберите другую дату.'})
    if cache.get('invalid_file'):
        return render(request, 'burst_app/error.html', {'error': 'Выбран некорретный формат файла, либо таблиц. Поддерживается .XLSX'})


@login_required
def report(request):
    monthpicker_form = MonthPickerForm()
    if request.method == "POST":
        try:
            filehandle = request.FILES.get('myfile',False)
            month, year = list(map(int, (request.POST['date'].split('/'))))
            month_dict = {month: index for index, month in enumerate(calendar.month_abbr) if month}
            month_name = next(k for k, v in month_dict.items() if v == month)
            rate = [(row.isp, row.quote, row.cost) for row in Rate.objects.all()]
            with ParseBurst(filehandle.temporary_file_path(), month, year) as inst:
                correct_result = []
                incorrect_result = []
                inst.set_traffic_direction({'Rostelecom': 'IN'})
                result = inst.parse_report()
                counter = 0
                for line in result:
                    if 'fail' in line:
                        counter += 1
                        incorrect_result.append(line)
                    else:
                        q = [calculate_amount(row[1],line[2], row[2]) for row in rate if row[0] == line[0].strip()]
                        temp_list = q[0] if q else []
                        first, *rest = line
                        correct_result.append([first.strip(), *rest] + temp_list)
                if counter in range(len(result) - 2, len(result) + 1):
                    cache.set('empty_report', True, 120)
                    return redirect('error')
        except (TypeError, InvalidFileException) as err:
            # print(err)
            cache.set('invalid_file', True, 120)
            return redirect('error')
        [line.insert(0, i) for i, line in enumerate(correct_result, 1)]
        [line.insert(0, i) for i, line in enumerate(incorrect_result, 1)]
        cache.set('correct_result', correct_result, 120)
        cache.set('incorrect_result', incorrect_result, 120)
        cache.set('date', {'month': month_name, 'year': year}, 120)
        return redirect('result')
    return render(request, 'burst_app/report.html', {'monthpicker_form': monthpicker_form})


@login_required
def result(request):
    try:
        correct_result = cache.get('correct_result')
        incorrect_result = cache.get('incorrect_result')
        date = cache.get('date')
        create_xlsx(correct_result, incorrect_result, date, settings.MEDIA_ROOT / 'burst_report.xlsx')
    except KeyError:
        raise Http404('Get empty data. Generate a new report.')
    return render(request, 'burst_app/result.html', {'correct_result': correct_result, 'incorrect_result': incorrect_result, 'date': date})


@login_required
def download_xlsx(request): 
    filename = 'burst_report.xlsx'
    filepath = settings.MEDIA_ROOT / filename
    mime_type, _ = mimetypes.guess_type(filepath)
    with open(filepath, "rb") as excel:
        data = excel.read()
    response = HttpResponse(data, content_type=mime_type)
    response['Content-Disposition'] = f"attachment; filename={filename}"
    return response


@login_required
def rate(request):
    rate_rows = Rate.objects.all()
    form = RateForm()

    if request.method == "POST":
        json_result = json.loads(request.body)
        isp = json_result.get('isp')
        if json_result.get('del'):
            Rate.objects.filter(isp__iexact=isp).delete()
        else:
            quote = float(json_result.get('quote'))
            cost = float(json_result.get('cost'))
            instance = Rate(isp=isp, quote=quote, cost=cost)

            if not Rate.objects.filter(isp__iexact=isp):
                instance.save()
            else:
                Rate.objects.filter(isp__iexact=isp).update(quote=quote, cost=cost)
    return render(request, 'burst_app/rate.html', {'rate_rows': rate_rows, 'form': form})


class SignInLogin(LoginView):
    template_name = 'burst_app/login.html'
    form_class = LoginForm

    def form_valid(self, form):
        remember_me = form.cleaned_data['remember_me']  # get remember me data from cleaned_data of form
        if not remember_me:
            self.request.session.set_expiry(0)
            self.request.session.modified = True
        return super().form_valid(form)
