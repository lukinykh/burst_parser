from django.apps import AppConfig


class BurstAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'burst_app'
