from django import forms
from django.contrib.auth.forms import AuthenticationForm
from bootstrap_datepicker_plus import MonthPickerInput
from django.forms import widgets

from .models import Rate

class LoginForm(AuthenticationForm):
    remember_me = forms.BooleanField(required=False)
    

class MonthPickerForm(forms.Form):
    date = forms.DateField(required=True, label=False,
        widget=MonthPickerInput(format='%m/%Y',                 
        options={
                    "showClose": True,
                    "showClear": True,
                    "showTodayButton": True,
                    "useCurrent": False
                }),  
    )

class RateForm(forms.ModelForm):
    class Meta:
        model = Rate
        fields = ['isp', 'quote', 'cost']

        widgets = {
            'isp': forms.TextInput(attrs={'class': 'form-control'}),
            'quote': forms.NumberInput(attrs={'class': 'form-control', 'min': '0'}),
            'cost': forms.NumberInput(attrs={'class': 'form-control', 'min': '0'})
        }
        