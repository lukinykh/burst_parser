from django.urls import path
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.views.generic import RedirectView

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('main', views.main, name='main'),
    path('login', views.SignInLogin.as_view(), name='login'),
    path('logout', auth_views.LogoutView.as_view(), name='logout'),
    path('report', views.report, name='report'),
    path('result', views.result, name='result'),
    path('error', views.error, name='error'),
    path('download', views.download_xlsx, name='download'),
    path('rate', views.rate, name='rate'),
]
