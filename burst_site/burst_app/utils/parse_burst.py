from openpyxl import load_workbook, Workbook
from openpyxl.utils import get_column_letter
from calendar import monthrange
from datetime import datetime
from decimal import Decimal as D

from ..models import Rate


# Function realization
##
# def parse_single_isp(sheet, month, year):
#     required_days = [day for day in range(1, monthrange(year, month)[1] + 1)]
#     report_days = [date.day for i in sheet.iter_rows(min_col=6, max_col=6, min_row=7, values_only=True) if (date:=datetime.strptime(i[0], '%d.%m.%Y %H:%M')).month == month and date.year == year]
#     isp_name = sheet['B1'].value
#     assert isp_name, f'Missing isp name in {sheet.title} sheet'
#     assert sheet['E5'].value == 'OUT', 'Must be OUT value in row'
#     assert type(sheet['C7'].value) in (int, float), 'Must be int/float type in INPUT traffic column (Mb)'
#     assert type(sheet['G7'].value) in (int, float), 'Must be int/float type in OUTPUT traffic column (Mb)'
#     if len(difference:=set(required_days).difference(set(report_days))) == 0:
#         traffic_list = sorted([D(i[0]) for i in sheet.iter_rows(min_col=7, max_col=7, min_row=7, values_only=True)], reverse=True)
#         burst_value = traffic_list[int(len(traffic_list) * 0.05)]
#         return isp_name, float(burst_value)
#     else:
#         return f'{list(difference)} month days mismatch to isp ==> {isp_name}'


# def parse_report(path_to_file, month, year, change_dir_dict=None):
#     wb = load_workbook(path_to_file)
#     sheet_names = [i for i in wb.sheetnames]
#     isp_dict = {wb[line].title: {wb[line]['B1'].value: 'IN'} for line in sheet_names}
#     result_list = [parse_single_isp(wb[sheet], month, year) for sheet in sheet_names]
#     wb.close()
#     return result_list
#
# if __name__ == '__main__':
#     parse_report('../../../test_data/2021_05_01_report_b2o_burst_NEW.XLSX', 4, 2021)


class ParseBurst:
    def __init__(self, path_to_file, month, year):
        self.month = month
        self.year = year
        self.wb = load_workbook(path_to_file)
        self.sheet_names = [i for i in self.wb.sheetnames]
        self.isp_dict = {
            self.wb[line].title: {self.wb[line]["B1"].value: "OUT"}
            for line in self.sheet_names
        }
        
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.wb.close()

    def _parse_single_isp(self, sheet, month, year, traffic_direction="OUT"):
        required_days = [day for day in range(1, monthrange(year, month)[1] + 1)]
        report_days = [
            date.day
            for i in sheet.iter_rows(min_col=6, max_col=6, min_row=7, values_only=True)
            if (date := datetime.strptime(i[0], "%d.%m.%Y %H:%M")).month == month
            and date.year == year
        ]
        isp_name = sheet["B1"].value
        description = sheet["B3"].value
        assert isp_name, f"Missing isp name in {sheet.title} sheet"
        assert sheet["E5"].value == "OUT", "Must be OUT value in row"
        assert type(sheet["C7"].value) in (
            int,
            float,
        ), "Must be int/float type in INPUT traffic column (Mb)"
        assert type(sheet["G7"].value) in (
            int,
            float,
        ), "Must be int/float type in OUTPUT traffic column (Mb)"
        if len(difference := set(required_days).difference(set(report_days))) == 0:
            if traffic_direction == "IN":
                traffic_list = sorted(
                    [
                        D(i[0])
                        for i in sheet.iter_rows(
                            min_col=3, max_col=3, min_row=7, values_only=True
                        )
                    ],
                    reverse=True,
                )
            elif traffic_direction == "OUT":
                traffic_list = sorted(
                    [
                        D(i[0])
                        for i in sheet.iter_rows(
                            min_col=7, max_col=7, min_row=7, values_only=True
                        )
                    ],
                    reverse=True,
                )
            burst_value = traffic_list[int(len(traffic_list) * 0.05)]
            return [isp_name, traffic_direction, float(burst_value), description]
        else:
            difference = list(difference)
            if difference == list(range(difference[0], difference[-1] + 1)):
                days = f"{difference[0]}-{difference[-1]}"
            else:
                days = ", ".join(list(map(str, difference)))
            return [isp_name, "fail", days, description]

    def set_traffic_direction(self, change_dir_dict):
        for values in self.isp_dict.values():
            for name, _ in values.items():
                for k, v in change_dir_dict.items():
                    if k == name:
                        values[name] = v
        return self.isp_dict

    def parse_report(self):
        return [
            self._parse_single_isp(
                self.wb[sheet],
                self.month,
                self.year,
                self.isp_dict[self.wb[sheet].title][self.wb[sheet]["B1"].value],
            )
            for sheet in self.sheet_names
        ]


def create_xlsx(correct, incorrect, date, path):
    wb = Workbook()
    ws1 = wb.active
    ws1.title = "report"
    ws1.merge_cells('A1:H1')
    ws1['A1'] = f"Отчет Burst за {date['month']}. {date['year']}"
    ws1.append(['#', 'ISP', 'Traffic direction', 'Burst (Mb)', 'Channel description', 'Quote (Mb)', 'Excess (Mb)', 'Amount (₽)'])
    for line in correct:
        ws1.append(line)
    ws1.cell(ws1.max_row + 1, 1)
    ws1.append(['#', 'ISP','Report status', 'Days mismatch to full month'])
    for line in incorrect:
        ws1.append(line)
    for col in range(1, ws1.max_column + 1):
        width = max([len(row[0]) for row in ws1.iter_rows(min_col=col, max_col=col, min_row=1, values_only=True) if row[0] and type(row[0]) == str]) + 3
        ws1.column_dimensions[get_column_letter(col)].width = width
    ws1.column_dimensions[get_column_letter(1)].width = 5
    wb.save(path)
    wb.close()


def calculate_amount(quote, burst, rate):
    for line in (quote, burst, rate):
        assert type(line) in (int, float)
    excess = burst - quote
    if excess > 0 and quote > 0:
        return [quote, round(excess, 2), round(excess * rate, 2)]
    else:
        return [quote, 0, 0]
