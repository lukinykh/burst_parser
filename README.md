Create python virtual environment and install dependencies:

    python -m venv env
    source env/bin/activate
    pip install -r requirements.txt

Add requirements.txt

    pip freeze > requirements.txt
    
Install docker with postgres:

    mkdir -p $HOME/docker/volumes/postgres
    sudo docker pull postgres
    sudo apt install postgresql-client

Unnecessary start, docker only:

    sudo docker run --rm --name pgdocker -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres -e POSTGRES_DB=postgresdb -e USERMAP_UID=999 -e USERMAP_GID=999 -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data postgres
    
show containers:

    sudo docker ps -a
    
install docker-compose:

    sudo apt install docker-compose

Create docker-compose.yml:

    version: '3.1'

    volumes:
     pg_project:

    services:
      pg_db:
        image: postgres
        restart: always
        environment:
          - POSTGRES_PASSWORD=postgres
          - POSTGRES_USER=postgres
          - POSTGRES_DB=postgresdb
        volumes:
          - pg_project:/var/lib/postgresql/data
        ports:
          - ${POSTGRES_PORT:-5432}:5432

'restart: always' command will start postgres after reboot of VM

Start docker-compose:

    sudo docker-compose up -d

Connect postgres:

    psql -h 127.0.0.1 -U postgres -d postgresdb
    
Add DB and user:

    CREATE DATABASE
    postgresdb=# create user burst_user with encrypted password 'my_password';
    CREATE ROLE
    postgresdb=# grant all privileges on database burst_db to burst_user;
    GRANT
    postgresdb=# 
    
Add basic migrations and start server (default port 8000):

    python3 manage.py migrate
    python3 manage.py runserver
    
Django Deployment checklist:

    python manage.py check --deploy
    

Install Gunicorn

    pip install gunicorn
    
Testing, from folder with manage.py:

    gunicorn burst_site.wsgi


Add systemctl Gunicorn service:

    sudo nano /etc/systemd/system/gunicorn.service
    
File contains:

    [Unit]
    Description=gunicorn daemon
    After=network.target
    
    [Service]
    User=linux_user
    Group=www-data
    WorkingDirectory=/home/linux_user/burst_prod/burst_site
    ExecStart=/home/linux_user/burst_prod/env/bin/gunicorn --access-logfile - --workers 3 --bind unix:/home/linux_user/burst_prod/burst_site/burst_site.sock burst_site.wsgi:application
    
    [Install]
    WantedBy=multi-user.target
    
Add service start after reboot:

    sudo systemctl start gunicorn
    sudo systemctl enable gunicorn

Check the existence of Unix socket file burst_site.sock:

    ls -l /home/linux_user/burst_prod/burst_site/
    total 12
    drwxrwxr-x 7 linux_user linux_user 4096 Jul  6 06:50 burst_app
    drwxrwxr-x 3 linux_user linux_user 4096 Jul  6 07:28 burst_site
    srwxrwxrwx 1 linux_user linux_user    0 Jul  6 08:21 burst_site.sock
    -rwxrwxr-x 1 linux_user linux_user  666 Jul  6 06:33 manage.py
    
Install Nginx:

    sudo apt install nginx
    
Add configuration file:

    sudo nano /etc/nginx/sites-available/burst_site
    
Text contains:

    server {
        listen 80;
        server_name burst.example.com;
    
        location = /burst_app/assets/img/favicon.ico { access_log off; log_not_found off; }
        location /static/ {
            root /home/linux_user/burst_prod/burst_site/burst_app;
        }
    
        location / {
            include proxy_params;
            proxy_pass http://unix:/home/linux_user/burst_prod/burst_site/burst_site.sock;
        }
    
    }


Enable and testing Nginx configuration:

    sudo ln -s /etc/nginx/sites-available/burst_site /etc/nginx/sites-enabled
    sudo nginx -t
    
Reload Nginx service, add firewall rules (Ubuntu):

    sudo systemctl restart nginx
    sudo ufw allow 'Nginx Full'
    
Create SSL bundle instruction:

    https://support.globalsign.com/ssl/ssl-certificates-installation/install-certificate-nginx

